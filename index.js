#!/usr/bin/env node

const program = require('commander')
const colors = require('colors')
const service = require('./service')

function gateWay(value, somevalue) {
	let options = {
		module: 'mangaid',
		keyword: null,
		url: null
	}
	let listArguments = Array.from(arguments)

	listArguments.filter((argument) => {
		return (typeof argument == "string")
	}).forEach((argument) => {
		if (argument.toString().startsWith('_')) {
			options.module = argument.replace('_', '')
		} else if (argument.toString().startsWith('https://') || argument.toString().startsWith('http://')) {
			options.url = argument
		} else if (typeof argument == "string") {
			options.keyword = argument
		}
	})
	function next() {
		if (program.url) {
			if (options.url === null) throw new Error("value that input is not a valid url".red)

			downloadMangaWithUrl(options.url)
		} else if (program.download) {
			if (options.keyword === null) throw new Error("value that input is blank".red)

			downloadMangaWithKeyword(options.keyword)
		} else if (program.find) {
			if (options.keyword === null) throw new Error("value that input is blank".red)

			findManga(options.keyword)
		} else if (program.chapter) {
			if (options.url === null) throw new Error("value that input is not a valid url".red)

			showChapters(options.url)
		}
	}

	service.vendor(options.module, (err) => {
		if (!err) {
			next()
		} else {
			console.error(`[Mangrab]`.red + ` can't find module "${options.module}"`)
		}
	})
}
function findManga(keyword) {
	console.info(`[Mangrab]`.green + ` Finding Manga with Keyword : "${ keyword }"`)
	
	service.find(keyword, (results) => {
		if (results.length >= 1) {
			console.info(`[Mangrab]`.green + ` We have "${ results.length }" sugestions`)
			console.info()

			results.forEach((result) => {
				console.info(`[Mangrab]`.green + ` ${result.title.yellow} L:[${result.link}]`)
				if (result.genre) {
					console.info(`[Mangrab]`.green + ` ${result.genre}`)
				}
				console.info()
			})
		} else {
			console.info(`[Mangrab]`.red + ` Upps, we can't find manga with keyword "${ keyword }"`)
		}
	})
}
function showChapters(url) {
	console.info(`[Mangrab]`.green + ` Show Chapters from : "${url}"`)

	service.showChapters(url, (chapters) => {
		console.info("[Mangrab]".green + ` Total chapters is "${chapters.length.toString().yellow}"`)
		console.info()


		chapters.forEach((chapter) => {
			console.info("[Mangrab]".green + ` ${chapter.title.yellow}`)
		})
	})
}
function downloadMangaWithKeyword(keyword) {
	console.info(`[Mangrab]`.green + ` Downloading Manga with Keyword : "${ keyword }"`)

	service.find(keyword, (results) => {
		if (results.length >= 1) {
			let result = results[0]

			console.info(`[Mangrab]`.green + ` We have "${ results.length }" sugestions`)
			console.info(`[Mangrab]`.green + ` ${ result.title } L:[${ result.link }]`)
			console.info()

			service.downloadUrl(result.link)
		} else {
			console.info(`[Mangrab]`.red + ` Upps, we can't find manga with keyword "${ keyword }"`)
		}
	})
}
function downloadMangaWithUrl(url) {
	console.info(`[Mangrab]`.green + ` Downloading Manga with url : "${ url }"`)

	service.downloadUrl(url)
}


program
	.version('0.1.3')
	.option('-m --module', 'Module system')
	.option('-f --find', 'Find manga in our database')
	.option('-c --chapter', 'Show chapters list from link')
	.option('-u --url', 'set URL manga')
	.option('-d --download', 'Download manga with keyword')
	.action(gateWay);
  
program.parse(process.argv);