const request = require("request")
const cheerio = require("cheerio")
const Promise = require("bluebird")
const Async = require("async")
const _ = require("underscore")
const saveImage = require("save-image")
const fs = require('fs')
const path = require('path')

const Request = Promise.promisify(request.get)

function querySearch(keyword, first, callbacks) {
	Request(`http://mangaid.co/search?query=${keyword}`).then((results) => {
		let result = JSON.parse(results.body)
		let suggestions = []

		if (_.isArray(result.suggestions) && result.length != 0) {
			if (first) {
				let data = result.suggestions[0].data;

				callbacks(`http://mangaid.co/manga/${data}`)			
			} else {
				Async.each(result.suggestions, (manga, callback) => {
					suggestions.push(`http://mangaid.co/manga/${manga.data}`)

					callback(null)
				}, (err) => {

					if (!err) callbacks(suggestions)
				})
			}
		}	
	})
}
function getChaptersLink(url, callbacks) {
	Request(url).then((res) => {
		let $ = cheerio.load(res.body)
		let chapters = []
		
		Async.each($('ul.chapters li'), (chapter, callback) => {
			chapters.push({
				link: $(chapter).find('a').eq(0).attr('href'),
				title: $(chapter).find('a').eq(0).text()
			})
			
			callback(null)
		}, (err) => {
			
			if (!err) callbacks(_.uniq(chapters).reverse())
		})
	})
}
function getMangaChapter(url, callbacks) {
	let folderName = url.split('/')[4]

	Request(url).then((res) => {
		let $ = cheerio.load(res.body)
		let images = []

		Async.each($('img.img-responsive'), (image, callback) => {
			let img = $(image).attr('src').trim()
			
			if (!img.includes('nontonanime')) images.push({
				alt: $(image).attr('alt').trim(),
				src: img
			})
			
			callback(null)
		}, (err) => {
			doSaveImages(folderName, _.uniq(images), (paths) => {
				if (!err) callbacks("Done ==> " + folderName)
			})
		})
	})
}
function getUrl(url) {
  return 'https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy'
    + '?container=focus'
    + '&url=' + url
    ;
}
function doSaveImages(folder, images, callbacks) {
	let paths = []
	let pathFolder = `./Downloaded/${folder}`
	let chapter = ""
	let loopChap = 0

	do {
		try {
			chapter = images[loopChap].alt.match(/Chapter (\d+)/)[1]
		} catch(e) {
			console.error("Error gblk")
		}
		loopChap++
	} while ((_.isNull(chapter) && _.isUndefined(chapter)))

	if (!fs.existsSync(pathFolder)){
		fs.mkdirSync(pathFolder)
	}
	if (!fs.existsSync(pathFolder + "/chapter-" + chapter)){
		fs.mkdirSync(pathFolder + "/chapter-" + chapter)

		Async.each(images, (image, callback) => {
			let filename = path.basename(image.src)

			saveImage(getUrl(image.src), `./Downloaded/${folder}/chapter-${chapter}/${filename}`).then((result) => {
				console.info(`Downloaded ==> ${image.src.substr(0, 50)}...`)

				paths.push(result.path)

				callback(null)
			})
		}, (err) => {

			console.log("Done ===> Chapter " + chapter)

			if (!err) callbacks(paths)
		})
	} else {
		callbacks("It's already exists")
	}
}

// querySearch('dead tube', true, (results) => {
// 	console.log(results)
// })
let url = process.argv[2]
getChaptersLink(url, (chapters) => {
    console.log('Chapters')
    chapters.forEach((chapter) => {
    	getMangaChapter(chapter.link, (images) => {
			console.log('images')
			console.log(images)
		})
    })
})

	


