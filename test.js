const request = require('request')
const cheerio = require('cheerio')
const async = require('async')
const fs = require('fs')
const path = require('path')

// request({
// 	url: 'https://komikcast.com/chapter/spirit-blade-mountain-chapter-104-bahasa-indonesia/',
// 	method: 'GET',
// 	headers: {
// 		'User-Agent': 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/63.0.3239.84 Chrome/63.0.3239.84 Safari/537.36',
// 		'Origin': 'komikcast.com'

// 	}
// }, (err, res, body) => {
// 	let $ = cheerio.load(body)

// 	async.each($('#readerarea a > img'), (image, callback) => {
// 		console.log($(image).attr('src'))
// 		console.log($(image).attr('src'))

// 		callback(null)
// 	})
// })

let pathModule = "./libs/module"
let modulePath = []

fs.readdir(pathModule, function (err, files) {
	if (err) throw err

	let modules = files.map(function (file) {
		return {
			path: path.join(pathModule, file),
			module: file
		}
	}).filter(function (file) {
		return fs.statSync(file.path).isDirectory()
	}).filter(function (file) {
		try	{
			return fs.statSync(file.path + '/module.js').isFile()
		} catch(e) {
			return false
		}
	}).map(function (file) {
		return {
			name: file.module,
			path: path.join(file.path, 'module')
		}
	})

	console.log(modules)
});