const request = require("request")
const cheerio = require("cheerio")
const Promise = require("bluebird")
const Async = require("async")
const _ = require("underscore")
const saveImage = require("save-image")

const fs = require('fs')
const path = require('path')

function Module() {
	this.name = "komikcast"
	this.pathUrl = "https://komikcast.com"
	this.queryUrl = "https://komikcast.com/wp-admin/admin-ajax.php"

	this.searchOptions = function(keyword) {
		let body = `action=ajaxy_sf&sf_value=${keyword}&search=false`;
		
		return {
			url: this.queryUrl,
			method: "POST",
			headers: {
				"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
			},
			body: body
		}
	}
	this.grab = {
		chaptersParent: ".cl ul li",
		chapterLink: "a",
		chapterText: "a",
		imagesParent: '#readerarea img',
		imageLink: "",
		imageText: "",

	}
	this.getManga = function(url) {
		return url.split('/')[3]
	}
	this.getChapter = function(image) {
		return image.match(/[+-]?\d+(\.\d+)?/g)[0].replace("-", "")
	}
	this.getPageIndex = function(image) {
		return image.match(/[+-]?\d+(\.\d+)?/g)[0]
	}
	this.getImageName = function(image) {
		return path.basename(image.src)
	}
}
Module.prototype.findProcess = function(results, callbacks) {
	let suggestions = []
	
	if (_.isArray(results.komik[0].all) && results.length != 0) {
		Async.each(results.komik[0].all, (manga, callback) => {
			suggestions.push({
				title: manga.post_title,
				link: manga.post_link,
				genre: manga.post_genres
			})

			callback(null)
		}, (err) => {

			if (!err) callbacks(suggestions)
		})
	}	
}
Module.prototype.findChapters = function($, url, callbacks) {
	let chapters = []

	Async.each($(this.grab.chaptersParent), (chapter, callback) => {
		chapters.push({
			link: $(chapter).find(this.grab.chapterLink).eq(0).attr('href'),
			title: $(chapter).find(this.grab.chapterText).eq(0).text()
		})
		
		callback(null)
	}, (err) => {
		
		if (!err) callbacks({
			title: this.getManga(url),
			chapters: chapters
		})
	})
}
Module.prototype.findImages = function($, url, callbacks) {
	let images = []
	let imageParent = $(this.grab.imagesParent)

	Async.eachSeries(imageParent, (image, callback) => {
		let img = $(image).attr('src').trim()
		
		if (!img.includes('nontonanime')) images.push({
			alt: "page " + ((imageParent.index(image) * 1) + 1),
			src: img
		})
		
		callback(null)
	}, (err) => {
		
		callbacks(images, this.getManga(url))
	})
}

module.exports = new Module()