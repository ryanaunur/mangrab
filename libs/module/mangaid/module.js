const request = require("request")
const cheerio = require("cheerio")
const Promise = require("bluebird")
const Async = require("async")
const _ = require("underscore")
const saveImage = require("save-image")

const fs = require('fs')
const path = require('path')

const Request = Promise.promisifyAll(request)

function Module() {
	this.name = "mangaid"
	this.pathUrl = "http://mangaid.co/manga"
	this.queryUrl = "http://mangaid.co/search?query="

	this.searchOptions = function(keyword) {
		return this.queryUrl + keyword
	}
	this.grab = {
		chaptersParent: "ul.chapters li",
		chapterLink: "a",
		chapterText: "a",
		imagesParent: 'img.img-responsive',
		imageLink: "",
		imageText: "",

	}
	this.getManga = function(url) {
		return url.split('/')[4]
	}
	this.getChapter = function(image) {
		return image.match(/[+-]?\d+(\.\d+)?/g)[0]
	}
	this.getPageIndex = function(image) {
		return image.match(/[+-]?\d+(\.\d+)?/g)[1]
	}
	this.getImageName = function(image) {
		let fileExt = path.extname(image.src)
		let indexing = this.getPageIndex(image.alt)

		return path.basename(indexing + fileExt)
	}
}
Module.prototype.findProcess = function(results, callbacks) {
	let suggestions = []
	
	if (_.isArray(results.suggestions) && results.length != 0) {
		Async.each(results.suggestions, (manga, callback) => {
			suggestions.push({
				title: manga.value,
				link: `${ this.pathUrl }/${ manga.data }`
			})

			callback(null)
		}, (err) => {

			if (!err) callbacks(suggestions)
		})
	}	
}
Module.prototype.findChapters = function($, url, callbacks) {
	let chapters = []

	Async.each($(this.grab.chaptersParent), (chapter, callback) => {
		chapters.push({
			link: $(chapter).find(this.grab.chapterLink).eq(0).attr('href'),
			title: $(chapter).find(this.grab.chapterText).eq(0).text()
		})
		
		callback(null)
	}, (err) => {
		
		if (!err) callbacks({
			title: this.getManga(url),
			chapters: chapters
		})
	})
}
Module.prototype.findImages = function($, url, callbacks) {
	let images = []

	Async.mapSeries($(this.grab.imagesParent), (image, callback) => {
		let img = $(image).attr('src').trim()
		
		if (!img.includes('nontonanime')) images.push({
			alt: $(image).attr('alt').trim(),
			src: img
		})
		
		callback(null)
	}, (err) => {
		
		callbacks(images, this.getManga(url))
	})
}

module.exports = new Module()