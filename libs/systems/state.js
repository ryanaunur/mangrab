const fs = require('fs')
const path = require('path')

function state() {
    this.pathProfile = path.join(__dirname, '../../profiles.json')
    this.pathModules = path.join(__dirname, '../')

    this.default = {
        use: "mangaid",
        timestamp: new Date().toLocaleString()
    }
}
state.prototype.get = function() {
    if (!fs.existsSync(this.pathProfile)) {
        fs.writeFileSync(this.pathProfile, JSON.stringify(this.default))
    
        return this.default.use
    } else {

        let profile = JSON.parse(fs.readFileSync(this.pathProfile))

        return profile.use
    }
}
state.prototype.use = function(module) {
    if (!fs.existsSync(this.pathProfile)) {
        fs.writeFileSync(this.pathProfile, JSON.stringify(this.default))
    
        return true
    }

    if (!fs.existsSync(this.pathModules + module)) throw new Error("Error, module not founds")

    fs.writeFileSync(this.pathProfile, JSON.stringify({
        use: module,
        timestamp: new Date().toLocaleString()
    }))

    return true
}

module.exports = new state()