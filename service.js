const request = require("request")
const cheerio = require("cheerio")
const Promise = require("bluebird")
const Async = require("async")
const _ = require("underscore")
const saveImage = require("save-image")
const fs = require('fs')
const path = require('path')
const colors = require('colors')

const Request = Promise.promisify(request)

let moduleManga = {

}

function readModule(callback) {
	let pathModule = "./libs/module"

	fs.readdir(pathModule, function (err, files) {
		if (err) throw err

		let modules = files.map(function (file) {
			return {
				path: path.join(pathModule, file),
				module: file
			}
		}).filter(function (file) {
			return fs.statSync(file.path).isDirectory()
		}).filter(function (file) {
			try	{
				return fs.statSync(file.path + '/module.js').isFile()
			} catch(e) {
				return false
			}
		}).map(function (file) {
			return {
				name: file.module,
				path: path.join(file.path, 'module.js')
			}
		})

		callback(modules)
	});
}
function setModule(useModule, next) {
	let isFind = false

	readModule((modulePath) => {
		modulePath.filter((module) => {
			return (module.name == useModule)
		}).forEach((item) => {
			moduleManga = require("./" + item.path)
			isFind = true

			console.info("[Mangrab]".green + ` Module set to "${item.name}"`)
			next(false)
		})

		if (!isFind) {
			next(true)
		}
	})
}
function dependModule(next) {
	readModule((modulePath) => {
		modulePath.forEach((module) => {
			modules[module.name] = require(module.path)
		})

		next()
	})
}

function errorHandling(e) {

	console.error("[Mangrab]".red + " error getting data from server", e)
}

/**
 * getUrl
 * @description re-building url with Google image cache proxy
 * 
 * @param {String} url 
 */
function getUrl(url) {

	return 'https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy'
		+ '?container=focus'
		+ '&url=' + url
}
/**
 * querySearch
 * @description find manga from search box website
 * 
 * @param {String} keyword 
 * @param {Function} callbacks 
 */
function querySearch(keyword, callbacks) {
	Request(moduleManga.searchOptions(keyword)).then((results) => {
		let result = JSON.parse(results.body)

		moduleManga.findProcess(result, callbacks)
	}).catch(errorHandling)
}
/**
 * getChaptersLink
 * @description get list of chapters link and title from website
 * 
 * @param {String} url 
 * @param {Function} callbacks 
 */
function getChaptersLink(url, callbacks) {
	console.time('[Mangrab]'.green + ' Downloading ' + url);
	
	Request(url).then((res) => {
		let $ = cheerio.load(res.body)
		
		moduleManga.findChapters($, url, (results) => {
			callbacks(_.uniq(results.chapters).reverse(), results.title)
		})
	}).catch(errorHandling)
}
/**
 * getMangaByChapter
 * @description get list of manga image from website
 * 
 * @param {String} url 
 * @param {Function} callbacks 
 */
function getMangaByChapter(url, rootUrl, callbacks) {

	Request(url).then((res) => {
		let $ = cheerio.load(res.body)
		
		moduleManga.findImages($, rootUrl, (results, folderName) => {

			doSaveImages(folderName, _.uniq(results), url, (err, paths) => {
				if (!err) callbacks("Done Downloaded " + paths.length + " images")
			})
		})
	}).catch(errorHandling)
}
/**
 * doSaveImages 
 * @description Save images from `images.Array` getMangaByChapter, Asyncronius function
 * 
 * @param {String} folder 
 * @param {Array} images 
 * @param {Function} callbacks 
 */
function doSaveImages(folder, images, url, callbacks) {
	let paths = []
	let chapter = ""
	let loopChap = 0
	let pathFolder = `./Downloaded/${folder}`

	chapter = moduleManga.getChapter(url)

	/** Make downloaded folder if it's not exists */
	if (!fs.existsSync('./Downloaded')) {
		fs.mkdirSync('./Downloaded')
	}

	if (!fs.existsSync(pathFolder)){
		fs.mkdirSync(pathFolder)
	}

	if (!fs.existsSync(`${pathFolder}/chapter-${chapter}`)){
		fs.mkdirSync(`${pathFolder}/chapter-${chapter}`)

		doAction()
	} else {
		let imageName = moduleManga.getImageName(images[0])
		
		if (!fs.existsSync(`${pathFolder}/chapter-${chapter}/${imageName}`)) {
			doAction()
		} else {
			callbacks(false, "It's already exists!")
		}
	}

	function doAction() {
		let $index = 1
		let totals = images.length

		Async.each(images, (image, callback) => {

			let filename = path.basename(image.src)
			let fileExt = path.extname(image.src)
			let indexing = moduleManga.getPageIndex(image.alt)

			let imageName = moduleManga.getImageName(image)

			if (!_isIklan(filename.toLowerCase())) {
				saveImage(getUrl(image.src), `./Downloaded/${folder}/chapter-${chapter}/${imageName}`).then((result) => {
					console.info(`[Mangrab]`.green + ` [${ $index }/${ totals }] Downloaded Chapters ${chapter} => ${ filename } [${ indexing }]` + " ✔ Done".green)

					paths.push(result.path)
					$index++

					callback(null)
				}).catch((e) => {
					console.info(`[Mangrab]`.green + ` [${ $index }/${ totals }] Downloaded Chapters ${chapter} => ${ filename }` + " ✖ Error".red)
					$index++

					callback(null)
				})
			} else {
				console.info(`[Mangrab]`.green + ` [${ $index }/${ totals }] Downloaded Chapters ${chapter} => ${ filename }` + " ✖ Iklan".yellow)
				$index++

				callback(null)
			}
		}, (err) => {

			if (!err) callbacks(err, paths)
		})
	}
}
/**
 * isIklan
 * @private
 * @description check is filename from url is contains words `iklan`
 * 
 * @param {String} filename 
 */
function _isIklan(filename) {
	
	return (filename.includes('iklan') || filename.includes('rekrut'))
}
function _isDone(manga) {
	let viewerEngine = fs.readFileSync(path.join(__dirname, 'viewer.html'))

	fs.writeFileSync(path.join(__dirname, `Downloaded/${manga}/viewer.html`), viewerEngine)
}

/**
 * downloadByKeyword
 * @description grab manga by keyword
 * 
 * @param {String} keyword 
 * @returns none
 */
function downloadByKeyword(keyword) {
	querySearch(keyword, (results) => {
		if (results.length != 0) {
			let url = results[0]

			getChaptersLink(url.link, (chapters, manga) => {
			    Async.mapSeries(chapters, (chapter, callback) => {
	    			console.info(`[Mangrab]`.green + ` Processing for Chapter ${ chapter.title }`)

			    	getMangaByChapter(chapter.link, url.link, (result) => {
						console.info("[Mangrab] ".green + result)
						
						callback(null)
					})
			    }, (err) => {
					console.info()
					console.timeEnd('[Mangrab]'.green + ' Downloading ' + url)
					console.info("[Mangrab]".green + " ✔ Done")
					
					_isDone(manga)
			    })
			})
		}
	})
}
/**
 * downloadByUrl
 * @description grab manga by URI
 * 
 * @param {String} url 
 * @returns none
 */
function downloadByUrl(url) {
	getChaptersLink(url, (chapters, manga) => {
	    Async.mapSeries(chapters, (chapter, callback) => {
	    	console.info(`[Mangrab]`.green + ` Processing for Chapter ${ chapter.title }`)

			console.log(chapter)

	    	getMangaByChapter(chapter.link, url, (result) => {
				console.info("[Mangrab] ".green + result)

				callback(null)
			})
		}, (err) => {
			console.info()
			console.timeEnd('[Mangrab]'.green + ' Downloading ' + url)
			console.info("[Mangrab]".green + " ✔ Done")

			_isDone(manga)
	    })
	})
}

module.exports = {
	vendor: setModule,
	find: querySearch,
	showChapters: getChaptersLink,
	download: downloadByKeyword,
	downloadUrl: downloadByUrl
}

	



