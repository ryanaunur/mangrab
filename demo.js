var Spinner = require('cli-spinner').Spinner;
var colors = require('colors')
var logUpdate = require('log-update');


const frames = ['1', '2', '3', '6', '10'];
let i = 0;

function loading() {
	var id = setInterval(() => {
		const frame = frames[i = ++i % frames.length];

		logUpdate(`Downloading ${frame} of 10`);

		if (frame == "10") {
			spinner.stop()
			logUpdate.clear()
			clearInterval(id)

			loading()
			spinner.start()
		}
	}, 600);
}
spinner = new Spinner({
	text: `%s`,
	stream: process.stderr
});
spinner.setSpinnerString(19);

spinner.start();
loading();




